Condicionais: if e else. 

/* var idade = prompt("Qual é a sua idade?");
//var idade = 18;
if(idade >= 18){
    alert("Maior de idade");
} else{ 
    alert("Menor de idade");
};*/

Laços de Repetição: for e while.

/*var count;
for(count=0; count <= 5; count++) {
    alert(count);
};*/

/*var count = 0;
while (count <= 5){
    console.log(count);
    count = count + 1; // ou count++;
};*/

Datas:

/*var d = new Date();
alert(d);
//alert(d.getTime());
//alert(d.getMinutes());
alert(d.getHours());
alert(d.getDay());
alert(d.getMonth()+1); // sempre vai precisar de +1 para trazer o mês certo.
alert(d.getFullYear());*/
